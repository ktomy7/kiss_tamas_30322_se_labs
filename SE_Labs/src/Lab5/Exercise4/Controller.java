package Lab5.Exercise4;

import Lab5.Exercise3.LightSensor;
import Lab5.Exercise3.TemperatureSensor;

import java.util.Timer;
import java.util.TimerTask;

public class Controller {
    TemperatureSensor temperatureSensor;
    LightSensor lightSensor;
    int counter = 0;

    private static Controller controller;

    private Controller() {
    }

    public static Controller getController() {
        if(controller == null)
            controller = new Controller();

        return controller;
    }

    public void control() {
        Timer timer = new Timer();
        timer.schedule(new TimerTask() {
            @Override
            public void run() {
                System.out.println("Temperature sensor value: " + temperatureSensor.readValue());
                System.out.println("Light sensor value: " + lightSensor.readValue());
                counter++;
                if(counter == 20)
                    timer.cancel();
            }
        }, 0, 1000);
    }
}
