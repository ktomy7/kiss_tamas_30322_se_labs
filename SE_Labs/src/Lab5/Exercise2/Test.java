package Lab5.Exercise2;

public class Test {
    public static void main(String[] args) {
        RealImage realImage = new RealImage("f1");
        RotatedImage rotatedImage = new RotatedImage("f2");

        realImage.display();
        rotatedImage.display();

        ProxyImage proxyImage1 = new ProxyImage(realImage, rotatedImage, true);
        ProxyImage proxyImage2 = new ProxyImage(realImage, rotatedImage, false);
    }
}
