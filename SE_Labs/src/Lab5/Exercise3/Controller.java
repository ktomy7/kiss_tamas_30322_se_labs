package Lab5.Exercise3;

import java.util.Timer;
import java.util.TimerTask;

public class Controller {
    TemperatureSensor temperatureSensor;
    LightSensor lightSensor;
    int counter = 0;

    public Controller() {
        this.temperatureSensor = new TemperatureSensor();
        this.lightSensor = new LightSensor();
    }

    public void control() {
        Timer timer = new Timer();
        timer.schedule(new TimerTask() {
            @Override
            public void run() {
                System.out.println("Temperature sensor value: " + temperatureSensor.readValue());
                System.out.println("Light sensor value: " + lightSensor.readValue());
                counter++;
                if(counter == 20)
                    timer.cancel();
            }
        }, 0, 1000);
    }
}
