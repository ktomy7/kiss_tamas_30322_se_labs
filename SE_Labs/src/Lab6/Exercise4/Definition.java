package Lab6.Exercise4;

public class Definition {
    private String description;

    public Definition() {}
    public Definition(String description) {
        this.description = description;
    }

    @Override
    public String toString() {
        return "Definition{" + description + "}";
    }
}
