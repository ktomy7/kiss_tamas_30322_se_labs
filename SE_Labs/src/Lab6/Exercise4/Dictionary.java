package Lab6.Exercise4;

import java.util.HashMap;

public class Dictionary {
    HashMap<Word, Definition> dictionary = new HashMap<>();

    public void addWord(Word w, Definition d) {
        dictionary.put(w, d);
    }

    public Definition getDefinition(Word w) {
        return dictionary.get(w);
    }

    public void getAllWords() {
        System.out.println("Words: ");
        for(Word w : dictionary.keySet()) {
            System.out.println(w);
        }
    }

    public void getAllDefinitions() {
        System.out.println("Definitions: ");
        for(Definition d : dictionary.values()) {
            System.out.println(d);
        }
    }

    @Override
    public String toString() {
        return "Dictionary{" +
                "dictionary=" + dictionary +
                '}';
    }
}
