package Lab6.Exercise4;

import java.util.Scanner;

public class ConsoleMenu {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        Dictionary dictionary = new Dictionary();

        while(true) {
            System.out.println();
            System.out.println("Dictionary");
            System.out.println("1.Add a new word to the dictionary");
            System.out.println("2.Get definition");
            System.out.println("3.Get all the words");
            System.out.println("4.Get all the definitions");
            System.out.println("0.Exit");

            int option = scanner.nextInt();
            switch (option) {
                case 1:
                    System.out.println("Specify the word and the definition: ");
                    String str = scanner.next();
                    Word w = new Word(str);
                    str = scanner.next();
                    Definition d = new Definition(str);
                    dictionary.addWord(w, d);
                    break;
                case 2:
                    System.out.println("Specify the word: ");
                    str = scanner.next();
                    Word word = new Word(str);
                    System.out.println(dictionary.getDefinition(word));
                    break;
                case 3:
                    dictionary.getAllWords();
                    break;
                case 4:
                    dictionary.getAllDefinitions();
                    break;
                case 0:
                    return;
                default:
                    System.out.println("Incorrect input!");
                    break;
            }
        }

    }
}
