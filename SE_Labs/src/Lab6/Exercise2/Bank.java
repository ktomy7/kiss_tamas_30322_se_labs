package Lab6.Exercise2;

import Lab6.Exercise1.BankAccount;

import java.util.ArrayList;
import java.util.Collections;

public class Bank {
    ArrayList<BankAccount> accounts = new ArrayList<>();

    public void addAccount(BankAccount bankAccount) {
        accounts.add(bankAccount);
    }

    public void printAccounts() {
        ArrayList<BankAccount> sorted = new ArrayList<>(accounts);
        Collections.sort(sorted);
        for(var account : sorted) {
            System.out.println(account);
        }
    }

    public void printAccounts(double minBalance, double maxBalance) {
        ArrayList<BankAccount> sorted = new ArrayList<>(accounts);
        Collections.sort(sorted);
        for(var account : sorted) {
            if (account.getBalance() > minBalance && account.getBalance() < maxBalance)
                System.out.println(account);
        }
    }

    public BankAccount getAccount(String owner) {
        for(var account : accounts) {
            if(account.getOwner().equals(owner))
                return account;
        }
        return null;
    }
}
