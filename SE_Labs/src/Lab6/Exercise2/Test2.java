package Lab6.Exercise2;

import Lab6.Exercise1.BankAccount;

public class Test2 {
    public static void main(String[] args) {
        Bank bank = new Bank();
        BankAccount bankAccount1 = new BankAccount("N", 100);
        BankAccount bankAccount2 = new BankAccount("N2", 70);
        BankAccount bankAccount3 = new BankAccount("N3", 430);
        BankAccount bankAccount4 = new BankAccount("N4", 50);
        BankAccount bankAccount5 = new BankAccount("N5", 360);
        BankAccount bankAccount6 = new BankAccount("N6", 530);

        bank.addAccount(bankAccount1);
        bank.addAccount(bankAccount2);
        bank.addAccount(bankAccount3);
        bank.addAccount(bankAccount4);
        bank.addAccount(bankAccount5);
        bank.addAccount(bankAccount6);

        bank.printAccounts();

        System.out.println("Accounts between $100-$400: ");
        bank.printAccounts(100, 400);

        System.out.println("Account with the owner 'Name3': ");
        System.out.println(bank.getAccount("Name3"));


    }
}
