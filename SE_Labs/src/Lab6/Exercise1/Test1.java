package Lab6.Exercise1;

public class Test1 {
    public static void main(String[] args) {
        BankAccount bankAccount1 = new BankAccount("Name", 100);
        BankAccount bankAccount2 = new BankAccount("Name2", 50);
        BankAccount bankAccount3 = new BankAccount("Name", 100);

        System.out.println(bankAccount1 + " is equal to " + bankAccount2 + ": " + bankAccount1.equals(bankAccount2));
        System.out.println(bankAccount1 + " is equal to " + bankAccount3 + ": " + bankAccount1.equals(bankAccount3));
    }
}
