package Lab6.Exercise3;

import Lab6.Exercise1.BankAccount;

import java.util.TreeSet;

public class Bank {
    TreeSet<BankAccount> accounts = new TreeSet<>();

    public void addAccount(BankAccount bankAccount) {
        accounts.add(bankAccount);
    }

    public void printAccounts() {
        for (var account : accounts) {
            System.out.println(account);
        }
    }

    public void printAccounts(double minBalance, double maxBalance) {
        for(var account : accounts) {
            if (account.getBalance() > minBalance && account.getBalance() < maxBalance)
                System.out.println(account);
        }
    }

    public BankAccount getAccount(String owner) {
        for(var account : accounts) {
            if(account.getOwner().equals(owner))
                return account;
        }
        return null;
    }
}
