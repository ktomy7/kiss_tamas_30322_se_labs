package Lab2;

import java.util.Scanner;

public class Exercise4 {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);

        System.out.println("Give the number of elements:");
        int N = scan.nextInt();
        int[] vector = new int[N];
        int max;

        for(int i=0; i < N; i++) {
            vector[i] = scan.nextInt();
        }

        max = vector[0];
        for(int i = 1; i < vector.length; i++) {
            if(vector[i] > max)
                max = vector[i];
        }
        System.out.println("The maximum number:" + max);
    }
}
