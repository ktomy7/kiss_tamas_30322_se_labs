package Lab2;

import java.util.Scanner;

public class Exercise6 {
    static int factorial(int n){
        if (n == 0)
            return 1;
        else
            return(n * factorial(n-1));
    }

    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);

        int N = scan.nextInt();
        int result = 1;

        //non-recursive method
        for(int i=N; i>0; i--) {
            result *= i;
        }
        System.out.println(result);

        //recursive methpd
        System.out.println(factorial(N));
    }
}
