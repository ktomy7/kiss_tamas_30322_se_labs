package Lab2;

import java.util.Random;

public class Exercise5 {
    public static void main(String[] args) {
        Random r = new Random();

        int[] vector = new int[10];

        for(int i=0; i < vector.length; i++) {
            vector[i] = r.nextInt(100);
        }

        for(int i=0; i < vector.length; i++)
            for(int j=0; j < vector.length-i-1; j++) {
                if(vector[j] > vector[j+1]) {
                    int aux = vector[j];
                    vector[j] = vector[j+1];
                    vector[j+1] = aux;
                }
            }

        for(int i=0; i < vector.length; i++) {
            System.out.println(vector[i]);
        }
    }
}
