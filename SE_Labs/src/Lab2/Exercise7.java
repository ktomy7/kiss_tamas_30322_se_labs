package Lab2;

import java.util.Random;
import java.util.Scanner;

public class Exercise7 {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        Random r = new Random();
        int number = r.nextInt(10);

        System.out.println("Guess the number!");

        for(int i=0; i < 3; i++) {
            System.out.println("Enter a number:");
            int guess = scan.nextInt();

            if(guess == number) {
                System.out.println("You guessed the number!");
                break;
            }
            else if(guess > number)
                System.out.println("Wrong answer, your number it too high");
            else if(guess < number)
                System.out.println("Wrong answer, your number is too low!");
            else
                System.out.println("You lost!");
        }
    }
}
