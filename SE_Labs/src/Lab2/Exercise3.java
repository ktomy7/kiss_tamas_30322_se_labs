package Lab2;

import java.util.Scanner;

public class Exercise3 {
    static boolean isPrime(int n) {
        if(n <= 1)
            return false;

        for(int i = 2; i < n; i++)
            if(n % i == 0)
                return false;

        return true;
    }

    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);

        int A = scan.nextInt();
        int B = scan.nextInt();
        int count = 0;

        for(int i = A; i <= B; i++) {
            if(isPrime(i)) {
                System.out.println(i);
                count++;
            }
        }
        System.out.println("Number of prime numbers:" + count);
    }
}
