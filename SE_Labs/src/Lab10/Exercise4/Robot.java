package Lab10.Exercise4;

import java.util.Objects;

public class Robot {
    private final String name;
    private Position position;

    public Robot(String name, Position position) {
        this.name = name;
        this.position = position;
    }

    public Robot(String name) {
        this.name = name;
        this.position = new Position(0, 0);
    }

    public Position getPosition() {
        return position;
    }

    public void setPosition(Position position) {
        this.position = position;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Robot robot = (Robot) o;
        return name.equals(robot.name) && getPosition().equals(robot.getPosition());
    }

    @Override
    public int hashCode() {
        return Objects.hash(name, getPosition());
    }

    public void move(MoveDirection move) {
        switch (move) {
            case Up -> this.getPosition().setY(this.getPosition().getY() - 1);
            case Down -> this.getPosition().setY(this.getPosition().getY() + 1);
            case Left -> this.getPosition().setX(this.getPosition().getX() - 1);
            case Right -> this.getPosition().setX(this.getPosition().getX() + 1);
        }
    }
}
