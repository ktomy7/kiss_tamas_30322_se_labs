package Lab11.Exercise1;

import java.text.DecimalFormat;
import java.util.Observable;
import java.util.Random;

public class TemperatureSensor extends Observable implements Runnable {
    Random random = new Random();

    float temp;
    Thread t;

    boolean active = true;
    boolean pause = false;

    public void start() {
        if (t == null) {
            t = new Thread(this);
            t.start();
        }
    }

    public float readValue() {
        return random.nextFloat(101);
    }

    @Override
    public void run() {
        while (active) {
            if (pause) {
                synchronized (this) {
                    try {
                        wait();
                    }
                    catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
            }
            temp = readValue();

            this.setChanged();
            this.notifyObservers();
        }
    }

    public void setPause(boolean p) {
        synchronized (this) {
            if (p) {
                pause = true;
            }
            else {
                pause = false;
                notify();
            }
        }
    }


    public float getTemp() {
        return temp;
    }

    public boolean isPaused() {
        return pause;
    }
}
