package Lab11.Exercise1;

public class Test {
    public static void main(String[] args) {
        TemperatureSensor sensor = new TemperatureSensor();
        sensor.start();

        View view = new View();
        Controller controller = new Controller(sensor, view);
    }
}
