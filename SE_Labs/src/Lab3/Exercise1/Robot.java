package Lab3.Exercise1;

public class Robot {
    int x; //position

    public Robot() {
        x = 1;
    }

    public void change(int k) {
        if(k >= 1)
            x += k;
    }

    @Override
    public String toString() {
        return "Position of robot: " + x;
    }
}
