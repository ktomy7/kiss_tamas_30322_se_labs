package Lab3.Exercise5;

import java.util.concurrent.Flow;

public class Flower {
    int petal;
    static int count;

    public Flower(){
        System.out.println("Flower has been created!");
        count++;
    }

    public static void main(String[] args) {
        Flower[] garden = new Flower[5];
        for(int i =0;i<5;i++){
            Flower f = new Flower();
            garden[i] = f;
        }
        Flower flower1 = new Flower();
        System.out.println(flower1.getCount());
    }

    public int getCount(){
        return count;
    }
}
