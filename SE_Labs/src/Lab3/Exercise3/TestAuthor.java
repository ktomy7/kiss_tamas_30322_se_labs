package Lab3.Exercise3;

public class TestAuthor {
    public static void main(String[] args) {
        Author author = new Author("My Name","myname@gmail.com",'m');

        System.out.println(author);

        author.setEmail("mynewemail@gmail.com");

        System.out.println(author.getEmail());
        System.out.println(author.getGender());
        System.out.println(author.getName());
    }
}
