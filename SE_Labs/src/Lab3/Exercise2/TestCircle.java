package Lab3.Exercise2;

public class TestCircle {
    public static void main(String[] args) {
        Circle c1 = new Circle(5.6);
        Circle c2 = new Circle(15.76, "blue");

        System.out.println(c1.getRadius());
        System.out.println(c1.getArea());

        System.out.println(c2.getRadius());
        System.out.println(c2.getArea());
    }
}
