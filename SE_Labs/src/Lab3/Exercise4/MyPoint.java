package Lab3.Exercise4;

public class MyPoint {
    int x;
    int y;

    public MyPoint(){
        x = 0;
        y = 0;
    }
    public MyPoint(int x, int y){
        this.x = x;
        this.y = y;
    }

    public void setX(int x) {
        this.x = x;
    }
    public void setY(int y) {
        this.y = y;
    }
    public int getX() {
        return x;
    }
    public int getY() {
        return y;
    }

    public void setXY(int x, int y) {
        this.x = x;
        this.y = y;
    }

    @Override
    public String toString() {
        return "(" + x + "," + y + ")";
    }

    public double distance(int x, int y) {
        double distance = Math.sqrt(Math.pow((this.x - x),2)+Math.pow((this.y - y),2));
        return distance;
    }

    public double distance(MyPoint another){
        double distance = Math.sqrt(Math.pow((this.x - another.x),2)+Math.pow((this.y - another.y),2));
        return distance;
    }
}
