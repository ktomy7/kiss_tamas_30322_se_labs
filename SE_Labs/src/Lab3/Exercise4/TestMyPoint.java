package Lab3.Exercise4;

public class TestMyPoint {
    public static void main(String[] args) {
        MyPoint p1 = new MyPoint();
        MyPoint p2 = new MyPoint(5,7);

        p1.setX(2);
        p1.setY(4);

        System.out.println(p2.getX());
        System.out.println(p2.getY());

        p2.setXY(8,3);

        System.out.println(p1);
        System.out.println(p2);

        System.out.println(p1.distance(9,10));
        System.out.println(p1.distance(p2));
    }
}
