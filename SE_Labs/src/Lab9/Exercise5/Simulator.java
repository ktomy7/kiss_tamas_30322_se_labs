package Lab9.Exercise5;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;

public class Simulator extends JFrame implements ItemListener {
    int currentX = 0;
    int currentY = 0;
    int counter = -1;
    Controller[] controllers = new Controller[10];
    JTextArea[] segments = new JTextArea[10];
    JLabel[] contNames = new JLabel[10];
    JButton addButton = new JButton();
    JTextField addText = new JTextField();
    JTextField addSegment = new JTextField();

    String[] stationsOptions = new String[10];
    JComboBox stationsCombo = new JComboBox();

    Simulator() {
        setTitle("Stations");
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setSize(900, 500);
        init();
        setVisible(true);
    }

    void init() {
        this.setLayout(null);

        addButton.setBounds(currentX + 10, currentY + 20, 150, 20);
        addButton.setText("Add train");

        addText.setBounds(currentX + 160, currentY + 20, 200, 20);
        addText.setEditable(true);

        addSegment.setBounds(currentX + 160, currentY + 50, 200, 20);
        addSegment.setEditable(true);

        stationsCombo.setBounds(currentX + 360, currentY + 20, 200, 20);

        stationsCombo.addItemListener(this);

        addButton.addActionListener(new ActionButtonAdd());

        add(addButton);
        add(addText);
        add(stationsCombo);
        add(addSegment);
        currentY += 50;
    }

    void addController(Controller cont) {
        counter++;
        stationsOptions[counter] = cont.stationName;
        stationsCombo.addItem(stationsOptions[counter]);

        contNames[counter] = new JLabel();
        segments[counter] = new JTextArea();
        controllers[counter] = cont;

        contNames[counter].setText(cont.stationName);
        contNames[counter].setBounds(currentX, currentY, 200, 20);

        segments[counter].setText(cont.displayStationState());
        segments[counter].setEditable(false);
        segments[counter].setBounds(currentX, currentY + 50, 1000, 100);

        add(contNames[counter]);
        add(segments[counter]);
        currentY += 150;
        refresh();
    }

    void refresh() {
        SwingUtilities.updateComponentTreeUI(this);
    }

    public void itemStateChanged(ItemEvent e) {

    }

    class ActionButtonAdd implements ActionListener {
        public void actionPerformed(ActionEvent e) {
            String stationName = stationsCombo.getSelectedItem().toString();
            String trainName = addText.getText().toString();
            String segmentName = addSegment.getText();
            int i;

            for (i = 0; i <= counter; i++) {
                if (controllers[i].stationName == stationName)
                    break;
            }
            int j;
            for (j = 0; j < controllers[i].list.size(); j++) {
                if (controllers[i].list.get(j).id == Integer.parseInt(segmentName))
                    break;
            }
            controllers[i].arriveTrain(new Train("", trainName), controllers[i].list.get(j).id);
            System.out.println(controllers[i].list.get(0).toString());
            segments[i].setText(controllers[i].displayStationState());
        }
    }
}
