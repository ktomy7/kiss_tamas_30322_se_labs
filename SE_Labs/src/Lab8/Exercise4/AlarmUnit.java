package Lab8.Exercise4;

public class AlarmUnit {
    private boolean ringing = false;

    public boolean isRinging() {
        return ringing;
    }

    public void setRinging(boolean ringing) {
        this.ringing = ringing;
    }
}
