package Lab8.Exercise4;

import java.util.ArrayList;

public class ControlUnit {
    private static ControlUnit singletonValue = null;

    private static final double MIN_TEMPERATURE = 20.0;
    private static final double MAX_TEMPERATURE = 25.0;

    private final ArrayList<FireSensor> fireSensors = new ArrayList<>();
    private final GsmUnit gsmUnit = new GsmUnit();

    private final AlarmUnit alarmUnit = new AlarmUnit();

    private final HeatingUnit heatingUnit = new HeatingUnit();
    private final CoolingUnit coolingUnit = new CoolingUnit();

    private ControlUnit() {}

    public static ControlUnit make() {
        if (singletonValue == null) {
            singletonValue = new ControlUnit();
        }
        return singletonValue;
    }

    public void handleEvent(Event event) {
        if (event instanceof FireEvent) {
            for (var fireSensor : this.fireSensors) {
                if (fireSensor.isActive()) {
                    alarmUnit.setRinging(true);
                    this.gsmUnit.alertOwner(event);
                }
            }
        } else if (event instanceof TemperatureEvent temperatureEvent) {
            if (temperatureEvent.getValue() < MIN_TEMPERATURE) {
                this.heatingUnit.setActive(true);
                this.coolingUnit.setActive(false);
            } else if (temperatureEvent.getValue() > MAX_TEMPERATURE) {
                this.heatingUnit.setActive(false);
                this.coolingUnit.setActive(true);
            }
        }
    }
}
