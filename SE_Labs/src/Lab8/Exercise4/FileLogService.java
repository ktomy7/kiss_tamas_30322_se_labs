package Lab8.Exercise4;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.text.SimpleDateFormat;
import java.util.Date;

import static java.nio.file.StandardOpenOption.APPEND;
import static java.nio.file.StandardOpenOption.CREATE;

public class FileLogService implements ILogService{
    public static String LOG_FILE = "src/Lab8/Exercise4/system_log.txt";

    @Override
    public void logEvent(Event event) {
        var typeName = "";
        try {
            this._writeToFile("New event: " + event);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void _writeToFile(String text) throws IOException {
        var date = new Date();
        var formatter = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss");

        Files.writeString(
                Path.of(LOG_FILE),
                "[" + formatter.format(date) + "] " + text + System.lineSeparator(),
                CREATE, APPEND);
    }
}
