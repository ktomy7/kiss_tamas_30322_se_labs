package Lab8.Exercise4;

public class CoolingUnit {
    private boolean active = false;

    public void setActive(boolean active) {
        this.active = active;
    }

    public boolean getActive() {
        return active;
    }
}
