package Lab8.Exercise4;

abstract class Event {

    EventType type;

    Event(EventType type) {
        this.type = type;
    }

    public EventType getType() {
        return type;
    }

}