package Lab8.Exercise3;

public class Simulator {

    /**
     * @param args
     */

    public static void main(String[] args) {

        // build station Cluj-Napoca
        Controler c1 = new Controler("Cluj-Napoca");

        var s1 = new Segment(1);
        var s2 = new Segment(2);
        var s3 = new Segment(3);

        c1.addControledSegment(s1);
        c1.addControledSegment(s2);
        c1.addControledSegment(s3);

        //  build station București
        var c2 = new Controler("București");

        var s4 = new Segment(4);
        var s5 = new Segment(5);
        var s6 = new Segment(6);

        c2.addControledSegment(s4);
        c2.addControledSegment(s5);
        c2.addControledSegment(s6);

        // Build station Iași
        var c3 = new Controler("Iași");

        var s7 = new Segment(7);
        var s8 = new Segment(8);

        c3.addControledSegment(s7);
        c3.addControledSegment(s8);

        // connect the 2 controllers

        c1.addNeighbourController(c2);
        c2.addNeighbourController(c1);
        c1.addNeighbourController(c3);
        c3.addNeighbourController(c1);
        c2.addNeighbourController(c3);
        c3.addNeighbourController(c2);

        //testing

        var t1 = new Train("București", "IC-001");
        s1.arriveTrain(t1);

        var t2 = new Train("Cluj-Napoca","R-002");
        s5.arriveTrain(t2);

        var t3 = new Train("București","IR-243");
        s7.arriveTrain(t3);


        c1.displayStationState();
        c2.displayStationState();
        c3.displayStationState();

        System.out.println("\nStart train control\n");

        //execute 3 times controller steps
        for(int i = 0; i < 3; i++){
            System.out.println("### Step " + i + " ###");
            c1.controlStep();
            c2.controlStep();
            c3.controlStep();

            System.out.println();

            c1.displayStationState();
            c2.displayStationState();
            c3.displayStationState();
        }
    }
}