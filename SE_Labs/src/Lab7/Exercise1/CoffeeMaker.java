package Lab7.Exercise1;

public class CoffeeMaker {
    public static final int maxCoffees = 10;
    private static int currentNumber = 0;

    Coffee makeCoffee() throws Exception {
        if (currentNumber >= maxCoffees) {
            throw new Exception("There can only be " + maxCoffees + " coffees created!");
        }

        ++currentNumber;

        System.out.println("Make a coffee");
        int t = (int) (Math.random() * 100);
        int c = (int) (Math.random() * 100);
        return new Coffee(t, c);
    }
}
