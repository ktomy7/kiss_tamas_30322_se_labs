package Lab7.Exercise1;

public class CoffeeTest {
    public static void main(String[] args) {
        CoffeeMaker maker = new CoffeeMaker();

        for (int i=0; i < CoffeeMaker.maxCoffees + 3; i++) {
            try {
                Coffee coffee = maker.makeCoffee();
                System.out.println(coffee);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }
}
