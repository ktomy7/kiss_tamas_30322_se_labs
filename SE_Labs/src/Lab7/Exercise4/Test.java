package Lab7.Exercise4;

import java.io.IOException;

public class Test {
    public static void main(String[] args) throws IOException, ClassNotFoundException {

        Car car = new Car("Tesla", 25000);

        car.saveToFile();

        car.readCar();
    }
}
