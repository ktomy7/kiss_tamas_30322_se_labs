package Lab7.Exercise4;

import java.io.*;
import java.util.Objects;

public class Car implements Serializable {
    private String model;
    private double price;

    public Car() {}
    public Car(String model, double price) {
        this.model = model;
        this.price = price;
    }

    @Override
    public String toString() {
        return "Car{" + model + ", $" + price + '}';
    }

    public void saveToFile() throws IOException {
        ObjectOutputStream outputStream = new ObjectOutputStream(new FileOutputStream("src\\Lab7\\Exercise4\\car.txt", true));

        outputStream.writeObject(this);
        System.out.println(this + " is saved to the file!");
        outputStream.close();
    }

    public void readCar() throws IOException, ClassNotFoundException {
        ObjectInputStream inputStream = new ObjectInputStream(new FileInputStream("src\\Lab7\\Exercise4\\car.txt"));

        Car car = (Car) inputStream.readObject();

        System.out.println(car);

        inputStream.close();
    }
}
