package Lab7.Exercise2;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;

public class CharacterCounter {
    private File file = new File("src\\Lab7\\Exercise2\\data.txt");
    private char character;
    private String text;

    public CharacterCounter(char character) {
        this.character = character;
    }

    public void count() {
        int count = 0;

        try {
            Scanner scanner = new Scanner(file);
            while (scanner.hasNextLine()) {
                text += scanner.nextLine();

            }
            scanner.close();
        } catch (FileNotFoundException e) {
            System.out.println("An error occurred!");
        }

        text = text.toLowerCase();
        char[] charArray = text.toCharArray();

        for (int i=0; i < charArray.length; i++) {
            if (charArray[i] == character)
                count++;
        }
        System.out.println("The character '" + character + "' appears in the file " + count + " times!");
    }
}
