package Lab7.Exercise2;

import java.util.Scanner;

public class Test {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        System.out.println("Enter a character: ");
        char character = scanner.next().charAt(0);
        CharacterCounter characterCounter = new CharacterCounter(character);
        characterCounter.count();

    }
}
