package Lab7.Exercise3;

import java.io.File;
import java.io.IOException;
import java.util.Scanner;

public class Test {
    public static void main(String[] args) throws IOException {
        Scanner scanner = new Scanner(System.in);
        Encrypter encrypter = new Encrypter();

        System.out.println("Do you want to do? encrypt or decrypt: ");
        String option = scanner.nextLine();

        switch (option) {
            case "encrypt": {
                System.out.println("What file do you want to encrypt? ");
                File file = new File(scanner.nextLine());

                String data = encrypter.readFile(file);
                encrypter.encryptText(data).writeToFile(file.getPath().replaceAll(".txt", ".enc"));
                }
                break;
            case "decrypt": {
                System.out.println("What file do you want to decrypt? ");
                File file = new File(scanner.nextLine());

                String data = encrypter.readFile(file);
                encrypter.decryptText(data).writeToFile(file.getPath().replaceAll(".enc",".dec"));
                }
                break;
            default: System.out.println("Incorrect input!");
        }
    }
}
