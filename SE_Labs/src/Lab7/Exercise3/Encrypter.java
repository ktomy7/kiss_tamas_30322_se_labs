package Lab7.Exercise3;

import java.io.*;
import java.util.Scanner;

public class Encrypter {
    private String encrypted;

    public Encrypter() {}

    public String readFile(File file) {
        String text = "";
        try {
            Scanner scanner = new Scanner(file);
            while (scanner.hasNextLine()) {
                text = text.concat(scanner.nextLine());
            }
            scanner.close();
        } catch (FileNotFoundException e) {
            System.out.println("An error occurred!");
        }
        return text;
    }

    public Encrypter encryptText(String data) {
        this.encrypted = data.chars()
                .map(ch -> ch - 1)
                .collect(StringBuilder::new, StringBuilder::appendCodePoint, StringBuilder::append)
                .toString();
        return this;
    }

    public Encrypter decryptText(String data) {
        this.encrypted = data.chars()
                .map(ch -> ch + 1)
                .collect(StringBuilder::new, StringBuilder::appendCodePoint, StringBuilder::append)
                .toString();
        return this;
    }

    public void writeToFile(String path) throws IOException {
        var myObj = new File(path);
        if (myObj.createNewFile()) {
            System.out.println("File created: " + myObj.getName());
        } else {
            System.out.println("File updated: " + myObj.getName());
        }
        Writer writer = new FileWriter(path);
        writer.write(this.encrypted);
        writer.close();
    }
}
