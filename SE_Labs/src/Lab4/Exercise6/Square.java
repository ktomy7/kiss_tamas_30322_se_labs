package Lab4.Exercise6;

public class Square extends Rectangle{
    //private double side; //the square inherits its attributes from the rectangle

    public Square() {
        super();
    }
    public Square(double side) {
        super(side,side);
        //this.side = side;
    }
    public Square(String color, boolean filled, double side) {
        super(color, filled, side, side);
    }

    public double getSide() {
       //return side;
       return super.getLength();
    }
    public void setSide(double side) {
        //this.side = side;
        super.setWidth(side); //because you can only access the public methods and the attributes of the rectangle are private
        super.setLength(side);
    }
    @Override
    public void setWidth(double side) {
        super.setWidth(side);
    }
    @Override
    public void setLength(double side) {
        super.setLength(side);
    }

    @Override
    public String toString() {
        return "“A Square with side=" + getSide() + ", which is a subclass of ”" + super.toString();
    }
}
