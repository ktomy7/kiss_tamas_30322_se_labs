package Lab4.Exercise5;

import Lab4.Exercise1.Circle;

public class Cylinder extends Circle {
    private double height;

    public Cylinder() {
        super();
        this.height = 1.0;

    }
    public Cylinder(double radius) {
        super(radius); //take care to pass the arguments to the right constructors and set the initial value for the other attributes of the same class
        this.height = 1.0;
    }
    public Cylinder(double radius, double height) {
        super(radius);
        this.height = height;
    }

    public double getHeight() {
        return height;
    }
    public double getVolume() {
        return Math.PI*getRadius()*getRadius()*height;
    }

    @Override
    public double getArea() {
        return 2*Math.PI*getRadius()*(getRadius()+height);
    }

}
